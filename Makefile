DOCNAME=cv
TEX=xelatex

all: $(DOCNAME).pdf

.PHONY: clean cont view

$(DOCNAME).pdf: $(DOCNAME).tex
	$(TEX) $<

cont: $(DOCNAME).tex
	ls $^ | entr make

view: $(DOCNAME).pdf
	evince $< &

clean:
	-rm *.blg *.bbl *.aux *.log *.out *.toc

